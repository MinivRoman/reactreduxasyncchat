# ReactReduxAsyncChat

## Start

1. *Start server.*
Move to folder **server** and execute commands: `yarn install` | `yarn start`.
2. *Start client.*
Move to folder **client** and execute commands: `npm install` | `npm run build` | `npm start`.
   
## Runtime

1. The client is connected to the server using a proxy (client package.json: `"proxy": "http://localhost:3001"`).

**Possible crash** - start another server that will change the client or server startup port (default ports: 3000 and 3001 in accordance)

2. Not found files. Solution - rename component folders with example: component - Component (capitalize first symbol in component folder names). *Git not see changes in folder name change :(*