const express = require('express');
const router = express.Router();

const { sign, authenticate, verify } = require('../middlewares/authenticate.middleware');
const { getUser } = require('../repositories/user.repository');

router.get('/', (req, res, next) => {
  res.send('GET home page.');
});

router.post('/api/login', (req, res, next) => {
  const { login, password } = req.body;

  if (login && password) {
    const user = getUser('login', login);
    if (!user) {
      return res.status(401).json({ err: "No such user found" });
    }

    if (user.password == password) {
      const payload = { login: user.login };
      const jwt = sign(payload);

      res.json({ jwt, payload });
    } else {
      res.status(401).json({ err: "Password is incorrect" });
    }
  } else {
    res.status(401).json({ err: "Not found data" });
  }
});

router.get('/api/login', authenticate, (req, res, next) => {
  const authorizationHeader = req.headers.authorization;
  const jwt = authorizationHeader.split(" ")[1];
  const user = verify(jwt);

  res.status(200).json({ userLogin: user.login });
});

router.get('/api/admin', authenticate, (req, res, next) => {
  const authorizationHeader = req.headers.authorization;
  const jwt = authorizationHeader.split(" ")[1];
  const user = verify(jwt);

  if (user.login !== "admin") {
    res.status(403).json({ err: "Access denied" });
  } else {
    res.status(200).json({});
  }
});

module.exports = router;