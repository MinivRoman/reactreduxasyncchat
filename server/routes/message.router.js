const express = require('express');
const router = express.Router();

const messageRepository = require('../repositories/message.repository');

/* GET */

router.get('/', (req, res, next) => {
    const messageList = messageRepository.getMessageList();

    if (messageList) {
        res.send(messageList);
    } else {
        res.sendStatus(500);
    }
});

router.get('/:id', (req, res, next) => {
    const id = req.params['id'];
    const message = messageRepository.getMessage('id', id);

    if (message) {
        res.send(message);
    } else {
        res.status(404).send('Message not found.');
    }
});

/* POST */

router.post('/', (req, res, next) => {
    if (!Object.keys(req.body).length) {
        return res.sendStatus(400);
    }

    const message = req.body;
    const addedMessage = messageRepository.addMessage(message);

    if (addedMessage) {
        res.send(addedMessage);
    } else {
        res.status(400).send('Invalid data.')
    }
});

/* PUT */

router.put('/:id', (req, res, next) => {
    if (!Object.keys(req.body).length) {
        return res.sendStatus(400);
    }

    const message = req.body;
    const updatedMessage = messageRepository.updateMessage(message);

    if (updatedMessage) {
        res.send(updatedMessage);
    } else {
        res.status(404).send('Message not found.');
    }
});

/* DELETE */

router.delete('/:id', (req, res, next) => {
    const id = req.params['id'];
    const message = messageRepository.deleteMessage(id);

    if (message) {
        res.send(message);
    } else {
        res.status(404).send('Message not found.');
    }
});

module.exports = router;
