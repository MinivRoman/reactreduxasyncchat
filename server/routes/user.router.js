const express = require('express');
const router = express.Router();

const userRepository = require('../repositories/user.repository');

/* GET */

router.get('/', (req, res, next) => {
  const userList = userRepository.getUserList();

  const newUserList = Object.assign([], userList);
  newUserList.shift();

  if (newUserList) {
    res.send(newUserList);
  } else {
    res.sendStatus(500);
  }
});

router.get('/:id', (req, res, next) => {
  const id = req.params['id'];
  const user = userRepository.getUser('id', id);

  if (user) {
    res.send(user);
  } else {
    res.status(404).send('User not found.');
  }
});

/* POST */

router.post('/', (req, res, next) => {
  if (!Object.keys(req.body).length) {
    return res.sendStatus(400);
  }

  const user = req.body;
  const addedUser = userRepository.addUser(user);

  if (addedUser) {
    res.send(addedUser);
  } else {
    res.status(400).send('Invalid data.')
  }
});

/* PUT */

router.put('/:id', (req, res, next) => {
  if (!Object.keys(req.body).length) {
    return res.sendStatus(400);
  }

  const user = req.body;
  const updatedUser = userRepository.updateUser(user);

  if (updatedUser) {
    res.send(updatedUser);
  } else {
    res.status(404).send('User not found.');
  }
});

/* DELETE */

router.delete('/:id', (req, res, next) => {
  const id = req.params['id'];
  const user = userRepository.deleteUser(id);

  if (user) {
    res.send(user);
  } else {
    res.status(404).send('User not found.');
  }
});

module.exports = router;
