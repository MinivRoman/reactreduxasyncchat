const fs = require('fs');

const getUserList = () => {
    try {
        const data = fs.readFileSync('./assets/userList.json', 'utf8');
        const userList = JSON.parse(data);

        return userList;
    } catch (err) {
        return false;
    }
}

const getUser = (propName, propValue) => {
    if (!propName || !propValue) {
        return false;
    }

    const userList = getUserList();
    const user = userList.find((user) => user[propName] == propValue);
    
    return user;
}

const addUser = (user) => {
    if (user && typeof user === 'object' && !Object.keys(user).length) {
        return false;
    }

    const userList = getUserList();
    
    const maxId = Math.max(...userList.map((user) => user.id));
    user.id = maxId + 1;

    userList.push(user);

    const data = JSON.stringify(userList);
    fs.writeFileSync('./assets/userlist.json', data);

    return userList[userList.length - 1];
}

const updateUser = (user) => {
    if (user && typeof user === 'object' && !Object.keys(user).length) {
        return false;
    }

    const userList = getUserList();
    
    const updatedUser = userList.find((u) => u.id == user.id);
    if (!updatedUser) {
        return false;
    }
    const updatedUserIndex = userList.indexOf(updatedUser);
    
    userList[updatedUserIndex] = { ...user };

    const data = JSON.stringify(userList);
    fs.writeFileSync('./assets/userlist.json', data);

    return userList[updatedUserIndex];
}

const deleteUser = (id) => {
    if (!id) {
        return false;
    }

    const userList = getUserList();
    
    const user = userList.find((user) => user.id == id);
    if (!user) {
        return false;
    }
    const userIndex = userList.indexOf(user);
    const deletedUser = userList.splice(userIndex, 1)[0];

    const data = JSON.stringify(userList);
    fs.writeFileSync('./assets/userlist.json', data);
    
    return deletedUser;
}

module.exports = {
    getUserList,
    getUser,
    addUser,
    updateUser,
    deleteUser
}
