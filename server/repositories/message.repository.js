const fs = require('fs');

const getMessageList = () => {
    try {
        const data = fs.readFileSync('./assets/messageList.json', 'utf8');
        const messageList = JSON.parse(data);

        return messageList;
    } catch (err) {
        return false;
    }
}

const getMessage = (propName, propValue) => {
    if (!propName || !propValue) {
        return false;
    }

    const messageList = getMessageList();
    const message = messageList.find((message) => message[propName] == propValue);

    return message;
}

const setTimeFormat = (unitTime) => {
    return `0${unitTime}`.slice(-2);
}

const setMessageDate = (date) => {
    const seconds = setTimeFormat(date.getSeconds());
    const minutes = setTimeFormat(date.getMinutes());
    const hours = setTimeFormat(date.getHours());
    const day = setTimeFormat(date.getDate());
    const month = setTimeFormat(date.getMonth() + 1);
    const year = date.getFullYear();

    const messageDate = `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`;
    return messageDate;
}

const addMessage = (message) => {
    if (message && typeof message === 'object' && !Object.keys(message).length) {
        return false;
    }

    const messageList = getMessageList();

    const maxId = Math.max(...messageList.map((message) => message.id));
    message.id = maxId + 1;
    message.created_at = setMessageDate(new Date());

    messageList.push(message);

    const data = JSON.stringify(messageList);
    fs.writeFileSync('./assets/messagelist.json', data);

    return messageList[messageList.length - 1];
}

const updateMessage = (message) => {
    if (message && typeof message === 'object' && !Object.keys(message).length) {
        return false;
    }

    const messageList = getMessageList();

    const updatedMessage = messageList.find((u) => u.id == message.id);
    if (!updatedMessage) {
        return false;
    }
    const updatedMessageIndex = messageList.indexOf(updatedMessage);
    messageList[updatedMessageIndex] = { ...message };

    const data = JSON.stringify(messageList);
    fs.writeFileSync('./assets/messagelist.json', data);

    return messageList[updatedMessageIndex];
}

const deleteMessage = (id) => {
    if (!id) {
        return false;
    }

    const messageList = getMessageList();

    const message = messageList.find((message) => message.id == id);
    if (!message) {
        return false;
    }
    const messageIndex = messageList.indexOf(message);
    const deletedMessage = messageList.splice(messageIndex, 1)[0];

    const data = JSON.stringify(messageList);
    fs.writeFileSync('./assets/messagelist.json', data);

    return deletedMessage;
}

module.exports = {
    getMessageList,
    getMessage,
    addMessage,
    updateMessage,
    deleteMessage
}
