const express = require('express');
const app = express();
const bodyParser = require('body-parser');

const indexRouter = require('./routes/index.router');
const userRouter = require('./routes/user.router');
const messageRouter = require('./routes/message.router');

const PORT = 3001;

app.use(bodyParser.json());
app.use(express.urlencoded({ extended: false }));

app.use('/', indexRouter);
app.use('/api/users', userRouter);
app.use('/api/messages', messageRouter);

app.listen(PORT, (err) => {
    if (err) {
        return console.error(`Error ${err}`);
    }
});