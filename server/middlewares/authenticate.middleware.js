const passport = require('passport');
const jwt = require('jsonwebtoken');
const passportJWT = require('passport-jwt');

const { getUser } = require('../repositories/user.repository');

let jwtOptions = {};

jwtOptions.jwtFromRequest = passportJWT.ExtractJwt.fromAuthHeaderAsBearerToken();
jwtOptions.secretOrKey = "react_redux_saga";

let strategy = new passportJWT.Strategy(jwtOptions, (jwt_payload, next) => {
    const user = getUser('login', jwt_payload.login);

    if (user) {
        next(null, user);
    } else {
        next(null, false);
    }
});

passport.use(strategy);

passport.initialize();

const sign = (payload) => jwt.sign(payload, jwtOptions.secretOrKey);
const authenticate = passport.authenticate("jwt", { session: false });

const verify = (token) => jwt.verify(token, jwtOptions.secretOrKey);

module.exports = {
    sign,
    authenticate,
    verify
}
