import { combineReducers } from 'redux';
import messageReducer from '../components/Chat/reducer';
import userReducer from '../components/User/reducer';

export default combineReducers({
    messageReducer,
    userReducer
});