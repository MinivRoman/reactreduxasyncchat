import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

import Login from '../Login';
import Chat from '../Chat';
import MessageEditor from '../Chat/messageEditor';
import UserList from '../User/userList';
import UserEditor from '../User/userEditor';

import './App.css';

function App() {
  return (
    <div className="App">
      <Switch>
        <Route exact path="/" component={Login} />
        <Route path="/chat" component={Chat} />
        <Route path="/messageEditor/:id" component={MessageEditor} />
        <Route path="/userList" component={UserList} />
        <Route exact path="/userEditor" component={UserEditor} />
        <Route path="/userEditor/:id" component={UserEditor} />
        <Redirect to="/" />
      </Switch>
    </div>
  );
}

export default App;
