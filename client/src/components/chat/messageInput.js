import React from 'react';
import { connect } from 'react-redux';
import * as ActionCreator from './messageActions';

class MessageInput extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            user: 'Roman',
            likes: 0,
            message: '',
            my: true
        }
    }

    onChangeMessage = (e) => {
        const message = e.target.value;
        this.setState({ message })
    }

    onClickSend = (e) => {
        this.setState({ message: '' });

        const message = Object.assign({}, this.state);
        this.props.addMessage(message);
    }

    render() {
        return (
            <div className="message-input">
                <textarea placeholder="Enter message..." value={this.state.message} onChange={this.onChangeMessage}></textarea>
                <button className="send" onClick={this.onClickSend}>
                    <span className="send-symbol">&#8679;</span> Send
                </button>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {}
};

const mapDispatchToProps = (dispatch) => {
    return {
        addMessage: (newMessage) => dispatch(ActionCreator.addMessage(newMessage))
    }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MessageInput);