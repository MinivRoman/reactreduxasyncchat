import * as ActionType from './actionTypes';

export const getMessageList = (messageList) => ({
    type: ActionType.GET_MESSAGE_LIST,
    payload: {
        messageList
    }
});

export const addMessage = (newMessage) => ({
    type: ActionType.ADD_MESSAGE,
    payload: {
        newMessage
    }
});

export const updateMessage = (updatedMessage) => ({
    type: ActionType.UPDATE_MESSAGE,
    payload: {
        updatedMessage
    }
});

export const deleteMessage = (deletedMessage) => ({
    type: ActionType.DELETE_MESSAGE,
    payload: {
        deletedMessage
    }
});