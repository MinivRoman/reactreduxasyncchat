import * as ActionTypes from './actionTypes';

const initialState = {
    messageList: []
}

const messageReducer = (state = initialState, action) => {
    switch (action.type) {
        case ActionTypes.GET_MESSAGE_LIST:
            {
                return {
                    ...state,
                    messageList: action.payload.messageList
                };
            }
        case ActionTypes.ADD_MESSAGE_SUCCESS:
            {
                return {
                    ...state,
                    messageList: [...state.messageList, action.payload.newMessage]
                };
            }
        case ActionTypes.UPDATE_MESSAGE_SUCCESS:
            {
                const updatedMessage = action.payload.updatedMessage;
                const messageList = Object.assign([], state.messageList);

                const updatedMessageList = messageList.map((message) => {
                    if (message.id == updatedMessage.id) {
                        return updatedMessage;
                    }
                    return message;
                });

                return {
                    ...state,
                    messageList: [...updatedMessageList]
                };
            }
        case ActionTypes.DELETE_MESSAGE_SUCCESS:
            {
                const deletedMessage = action.payload.deletedMessage;
                const messageList = Object.assign([], state.messageList);
                const newMessageList = messageList.filter((message) => message.id != deletedMessage.id);

                return {
                    ...state,
                    messageList: [...newMessageList]
                };
            }
        default:
            return state;
    }
}

export default messageReducer;