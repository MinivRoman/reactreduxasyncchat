import axios from 'axios';
import api from '../../configs/api';
import { call, put, takeEvery, all } from 'redux-saga/effects';
import * as MessageActions from './actionTypes';

function* addMessage(action) {
    try {
        const newMessage = yield call(axios.post, `${api.url}/messages`, action.payload.newMessage);
        
        yield put({
            type: MessageActions.ADD_MESSAGE_SUCCESS,
            payload: {
                newMessage: newMessage.data
            }
        });
    } catch (err) {
        console.error(err.message);
    }
}

function* watchAddMessage() {
    yield takeEvery(MessageActions.ADD_MESSAGE, addMessage);
}

function* updateMessage(action) {
    try {
        const updatedMessage = yield call(
            axios.put,
            `${api.url}/messages/${action.payload.updatedMessage.id}`,
            action.payload.updatedMessage
        );
        yield put({
            type: MessageActions.UPDATE_MESSAGE_SUCCESS,
            payload: {
                updatedMessage: updatedMessage.data
            }
        });
    } catch (err) {
        console.error(err.message);
    }
}

function* watchUpdateMessage() {
    yield takeEvery(MessageActions.UPDATE_MESSAGE, updateMessage);
}

function* deleteMessage(action) {
    try {
        const deletedMessage = yield call(
            axios.delete,
            `${api.url}/messages/${action.payload.deletedMessage.id}`
        );

        yield put({
            type: MessageActions.DELETE_MESSAGE_SUCCESS,
            payload: {
                deletedMessage: deletedMessage.data
            }
        });
    } catch (err) {
        console.error(err.message);
    }
}

function* watchDeleteMessage() {
    yield takeEvery(MessageActions.DELETE_MESSAGE, deleteMessage);
}

export default function* messageSagas() {
    yield all([
        watchAddMessage(),
        watchUpdateMessage(),
        watchDeleteMessage()
    ]);
}