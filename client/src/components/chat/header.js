import React from 'react';

const ChatView = (props) => {
    return (
        <header>
            <div className="title">Chat room</div>
            <div className="number-participants"><span>{props.numberParticipants}</span> participants</div>
            <div className="number-messages"><span>{props.numberMessages}</span> messages</div>
            <div className="last-message">last message at {props.timeLastMessage}</div>
        </header>
    );
}

export default ChatView;
