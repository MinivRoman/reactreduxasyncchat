import React from 'react';

const MessageView = (props) => {
    const { message } = props;
    
    return (
        <div
            key={message.id}
            data-message-id={message.id}
            className={`message${message.my ? ' my' : ''}`}
            onClick={props.onClickMessage}
        >
            {
                message.my ?
                    null :
                    <img className="avatar" src={message.avatar} alt="avatar" />
            }
            <div className="description">
                <p className="text">{message.message}</p>
                <div className="meta-info">
                    <div className="like">
                        <span className="symbol symbol-like">&#10004;</span>
                        <span>{message.likes}</span>
                        {
                            message.my ?
                                <span>
                                    <span className="symbol symbol-update">&#9998;</span>
                                    <span className="symbol symbol-delete">&#9746;</span>
                                </span> : null
                        }
                    </div>
                    <div className="date">{message.created_at}</div>
                </div>
            </div>
        </div>
    );
}

export default MessageView;
