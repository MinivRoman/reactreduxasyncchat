import React from 'react';
import { connect } from 'react-redux';
import * as ActionCreator from './messageActions';
import MessageView from './messageView';

class MessageList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            updateMessage: false,
            messageId: null,
            messageText: ''
        };
    }

    onClickLike = (messageId) => {
        const messageList = Object.assign([], this.props.messageList);
        const updatedMessage = messageList.find((message) => message.id == messageId);

        if (updatedMessage && !updatedMessage.my) {
            updatedMessage.likes++;
        }

        this.props.updateMessage(updatedMessage);
    }

    onChangeMessageTextUpdate = (e) => {
        const messageText = e.target.value;
        this.setState({ messageText });
    }

    onDeleteMessage = (messageId) => {
        const messageList = Object.assign([], this.props.messageList);
        const deletedMessage = messageList.find((message) => message.id == messageId);

        this.props.deleteMessage(deletedMessage);
    }

    onClickMessage = (e) => {
        const messageId = e.currentTarget.dataset.messageId;

        if (e.target.classList.contains('symbol-like')) {
            this.onClickLike(messageId);
        }
        else if (e.target.classList.contains('symbol-update')) {
            this.props.history.push(`/messageEditor/${messageId}`);
        }
        else if (e.target.classList.contains('symbol-delete')) {
            this.onDeleteMessage(messageId);
        }
    }

    render() {
        return (
            <div className="message-list">
                {this.props.messageList.map((message) =>
                    <MessageView
                        message={message}
                        onClickMessage={this.onClickMessage}
                    />
                )}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        messageList: state.messageReducer.messageList
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        updateMessage: (updatedMessage) => dispatch(ActionCreator.updateMessage(updatedMessage)),
        deleteMessage: (deletedMessage) => dispatch(ActionCreator.deleteMessage(deletedMessage))
    }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MessageList);
