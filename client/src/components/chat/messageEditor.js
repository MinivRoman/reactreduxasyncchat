import React from 'react';
import Modal from 'react-responsive-modal';
import { connect } from 'react-redux';
import * as ActionCreator from './messageActions';
import Loader from '../Loader';
import checkAuthenticate from '../../helpers/security/checkAuthenticate';

import './messageEditor.css';

class MessageEditor extends React.Component {
    componentDidMount() {
        checkAuthenticate(this.props.history);

        console.log(this.props.match);
        console.log(this.props.match.params.id);
        const messageId = this.props.match.params.id;

        fetch(`/api/messages/${messageId}`)
            .then((res) => res.json())
            .then((message) => {
                console.log(message);
                this.setState({ message });
            });

    }

    constructor(props) {
        super(props);

        this.state = {
            openModal: true,
            message: null
            // updateMessage: false,
            // messageId: null,
            // messageText: ''
        };
    }

    onOpenModal = () => {
        this.setState({ openModal: true });
    }

    onCloseModel = () => {
        this.props.history.push('/chat');
        // this.setState({ openModal: false });
    }

    onChangeMessage = (e) => {
        const message = Object.assign({}, this.state.message);
        message.message = e.target.value;
        this.setState({ message });
    }

    onClickUpdate = (e) => {
        // const newUser = Object.assign({}, this.state.activeUser);
        // console.log(newUser);
        // switch (this.mode) {
        //     case 'add':
        //         console.log('add');
        //         this.props.addUser(newUser);
        //         break;
        //     case 'update':
        //         this.props.updateUser(newUser);
        //         break;
        // }

        console.log(this.state.message);
        const updatedMessage = Object.assign({}, this.state.message);
        this.props.updateMessage(updatedMessage);

        this.onCloseModel();
    }

    render() {
        return (
            this.state.message ?
                <div className="message-editor">
                    <Modal
                        open={this.state.openModal}
                        onClose={this.onCloseModel}
                        center
                        classNames={{
                            overlay: 'overlay',
                            modal: 'modal'
                        }}
                    >
                        <div className="messageEditor">
                            <textarea
                                placeholder="Enter message..."
                                value={this.state.message.message}
                                onChange={this.onChangeMessage}
                            ></textarea>
                            <button className="confirm" onClick={this.onClickUpdate}>
                                <span className="send-symbol">&#8679;</span> Update
                        </button>
                        </div>
                    </Modal>
                </div> :
                <Loader />
        );
    }
}

const mapStateToProps = (state) => {
    return {
        // messageList: state.messageReducer.messageList
        // activeMessage: state.messageReducer.activeMessage
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        updateMessage: (updatedMessage) => dispatch(ActionCreator.updateMessage(updatedMessage)),
    }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MessageEditor);

// export default MessageEditor;