import React from 'react';
import Header from './header';
import MessageList from './messageList';
import MessageInput from './messageInput';

class ChatView extends React.Component {
    getNumberParticipants() {
        const userNameList = this.props.messageList.map((message) => {
            return message.user;
        });

        const numberParticipants = [...new Set(userNameList)].length;

        return numberParticipants;
    }

    getNumberMessages() {
        return this.props.messageList.length;
    }

    getTimeLastMessage() {
        const { messageList } = this.props;
        return messageList[messageList.length - 1].created_at.split(' ')[1];
    }

    render() {
        return (
            <div className="chat">
                <Header
                    numberParticipants={this.getNumberParticipants()}
                    numberMessages={this.getNumberMessages()}
                    timeLastMessage={this.getTimeLastMessage()}
                />
                <MessageList history={this.props.history} />
                <MessageInput />
            </div>
        );
    }
}

export default ChatView;
