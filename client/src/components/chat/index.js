import React from 'react';
import { connect } from 'react-redux';
import Loader from '../Loader';
import ChatView from './view';
import checkAuthenticate from '../../helpers/security/checkAuthenticate';
import * as ActionCreator from './messageActions';
import './chat.css';

class Chat extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            messageList: null
        };
    }

    componentDidMount() {
        checkAuthenticate(this.props.history);

        fetch('/api/messages')
            .then((res) => res.json())
            .then((messageList) => {
                this.props.getMessageList(messageList);
            });
    }

    render() {
        return (
            this.props.messageList.length !== 0 ?
                <ChatView
                    messageList={this.props.messageList}
                    history={this.props.history}
                /> :
                <Loader />
        );
    }
}

const mapStateToProps = (state) => {
    return {
        messageList: state.messageReducer.messageList
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        getMessageList: (messageList) => dispatch(ActionCreator.getMessageList(messageList))
    }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Chat);