import * as ActionTypes from './actionTypes';
import defaultUserConfig from '../../configs/defaultUserConfig';

const initialState = {
    userList: [],
    activeUser: defaultUserConfig
}

const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case ActionTypes.GET_USER_LIST:
            {
                return {
                    ...state,
                    userList: action.payload.userList
                };
            }
        case ActionTypes.SET_ACTIVE_USER:
            {
                return {
                    ...state,
                    activeUser: { ...action.payload.activeUser }
                };
            }
        case ActionTypes.ADD_USER_SUCCESS:
            {
                return {
                    ...state,
                    userList: [...state.userList, action.payload.newUser]
                };
            }
        case ActionTypes.UPDATE_USER_SUCCESS:
            {
                const updatedUser = action.payload.updatedUser;
                const userList = Object.assign([], state.userList);

                const updatedUserList = userList.map((user) => {
                    if (user.id == updatedUser.id) {
                        return updatedUser;
                    }
                    return user;
                });

                return {
                    ...state,
                    userList: [...updatedUserList]
                };
            }
        case ActionTypes.DELETE_USER_SUCCESS:
            {
                const deletedUser = action.payload.deletedUser;
                const userList = Object.assign([], state.userList);

                const newUserList = userList.filter((user) => user.id != deletedUser.id);

                return {
                    ...state,
                    userList: [...newUserList]
                };
            }
        default:
            return state;
    }
}

export default userReducer;