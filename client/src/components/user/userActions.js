import * as ActionType from './actionTypes';

export const getUserList = (userList) => ({
    type: ActionType.GET_USER_LIST,
    payload: {
        userList
    }
});

export const setActiveUser = (activeUser) => ({
    type: ActionType.SET_ACTIVE_USER,
    payload: {
        activeUser
    }
});

export const getUser = (id) => ({
    type: ActionType.GET_USER,
    payload: {
        id
    }
});

export const addUser = (newUser) => ({
    type: ActionType.ADD_USER,
    payload: {
        newUser
    }
});

export const updateUser = (updatedUser) => ({
    type: ActionType.UPDATE_USER,
    payload: {
        updatedUser
    }
});

export const deleteUser = (deletedUser) => ({
    type: ActionType.DELETE_USER,
    payload: {
        deletedUser
    }
});
