import React from 'react';
import Modal from 'react-responsive-modal';
import { connect } from 'react-redux';
import * as ActionCreator from './userActions';
import Input from '../Input';
import userFormConfig from '../../configs/userFormConfig';
import defaultUserConfig from '../../configs/defaultUserConfig';
import checkAccess from '../../helpers/security/checkAccess';

import './userEditor.css';

class UserEditor extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            activeUser: this.props.activeUser,
            openModal: true
        };

        this.mode = 'add';
    }

    componentDidMount() {
        checkAccess(this.props.history);
        
        const userId = this.props.match.params.id;
        if (userId) {
            this.mode = 'update';
            this.props.getUser(userId);
        }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.activeUser.id !== prevState.activeUser.id &&
            nextProps.match.params.id) {
            return {
                ...prevState,
                activeUser: {...nextProps.activeUser}
            }
        } else {
            return null;
        }
    }

    onOpenModal = () => {
        this.setState({ openModal: true });
    }

    onCloseModel = () => {
        this.props.setActiveUser(defaultUserConfig);
        this.props.history.push('/userList');
    }

    onChangeData = (e) => {        
        const activeUser = Object.assign({}, this.state.activeUser);
        activeUser[e.target.name] = e.target.value;
        
        this.setState({ activeUser });
    }

    onClickSend = (e) => {
        const newUser = Object.assign({}, this.state.activeUser);
        
        switch (this.mode) {
            case 'add':
                this.props.addUser(newUser);
                break;
            case 'update':
                this.props.updateUser(newUser);
                break;
        }

        this.onCloseModel();
    }

    render() {
        return (
            <div className="user-editor">
                <Modal
                    open={this.state.openModal}
                    onClose={this.onCloseModel}
                    center
                    classNames={{
                        overlay: 'overlay',
                        modal: 'modal'
                    }}
                >
                    <div className="userEditor">
                        {
                            userFormConfig.map((item, index) =>
                            <Input
                                label={item.label}
                                type={item.type}
                                value={this.state.activeUser[item.label]}
                                onChange={this.onChangeData}
                            />)
                        }
                        <button className="confirm" onClick={this.onClickSend}>
                            <span className="send-symbol">&#8679;</span> Send
                        </button>
                    </div>
                </Modal>
            </div> 
        );
    }
}

const mapStateToProps = (state) => {
    return {
        userList: state.userReducer.userList,
        activeUser: state.userReducer.activeUser
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        getUser: (id) => dispatch(ActionCreator.getUser(id)),
        setActiveUser: (activeUser) => dispatch(ActionCreator.setActiveUser(activeUser)),
        addUser: (newUser) => dispatch(ActionCreator.addUser(newUser)),
        updateUser: (updatedUser) => dispatch(ActionCreator.updateUser(updatedUser))
    }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(UserEditor);