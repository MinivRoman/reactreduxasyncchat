import React from 'react';

const UserListView = (props) => {
    return (
        <div className="user-list-wrap">
            <ul className="user-list">
                {
                    props.userList.map((user) =>
                        <li key={user.id} data-user-id={user.id} className="user" onClick={props.onClickUser}>
                            <div className="user-props">
                                <span>{user.name}</span>
                                <span>{user.surname}</span>
                                <span>{user.email}</span>
                            </div>
                            <div className="user-settings">
                                <button className="btn btn-update">Update</button>
                                <button className="btn btn-delete">Delete</button>
                            </div>
                        </li>
                    )
                }
            </ul>
            <button className="btn btn-add" onClick={props.onClickAddUser}>Add</button>
        </div>
    );
}

export default UserListView;