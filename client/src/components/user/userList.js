import React from 'react';
import { connect } from 'react-redux';
import * as ActionCreator from './userActions';
import Loader from '../Loader';
import UserListView from './userListView';
import checkAccess from '../../helpers/security/checkAccess';

import './userList.css';

class UserList extends React.Component {
    componentDidMount() {
        checkAccess(this.props.history);

        fetch('/api/users')
            .then((res) => res.json())
            .then((userList) => {
                console.log(userList);
                this.props.getUserList(userList);
            });
    }

    onClickAddUser = (e) => {
        this.props.history.push('/userEditor');
    }

    onClickUser = (e) => {
        if (e.target.classList.contains('btn-update')) {
            const userId = e.currentTarget.dataset.userId;
            this.props.history.push(`/userEditor/${userId}`);
        }
        else if (e.target.classList.contains('btn-delete')) {
            const userId = e.currentTarget.dataset.userId;
            const userList = Object.assign([], this.props.userList);

            const deletedUser = userList.find((user) => user.id == userId);

            this.props.deleteUser(deletedUser);
        }
    }

    render() {
        return (
            this.props.userList.length !== 0 ?
                <UserListView
                    userList={this.props.userList}
                    onClickAddUser={this.onClickAddUser}
                    onClickUser={this.onClickUser}
                /> :
                <Loader />
        );
    }
}

const mapStateToProps = (state) => {
    return {
        userList: state.userReducer.userList
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        getUserList: (userList) => dispatch(ActionCreator.getUserList(userList)),
        deleteUser: (deletedUser) => dispatch(ActionCreator.deleteUser(deletedUser))
    }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(UserList);