import axios from 'axios';
import api from '../../configs/api';
import { call, put, takeEvery, all } from 'redux-saga/effects';
import * as UserActions from './actionTypes';

function* getUser(action) {
    try {
        const user = yield call(axios.get, `${api.url}/users/${action.payload.id}`);
        yield put({
            type: UserActions.SET_ACTIVE_USER,
            payload: {
                activeUser: user.data
            }
        });
    } catch (err) {
        console.error(err.message);
    }
}

function* watchGetUser() {
    yield takeEvery(UserActions.GET_USER, getUser);
}

function* addUser(action) {
    try {
        const newUser = yield call(axios.post, `${api.url}/users`, action.payload.newUser);
        yield put({
            type: UserActions.ADD_USER_SUCCESS,
            payload: {
                newUser: newUser.data
            }
        });
    } catch (err) {
        console.error(err.message);
    }
}

function* watchAddUser() {
    yield takeEvery(UserActions.ADD_USER, addUser);
}

function* updateUser(action) {
    try {
        const updatedUser = yield call(
            axios.put,
            `${api.url}/users/${action.payload.updatedUser.id}`,
            action.payload.updatedUser
        );
        yield put({
            type: UserActions.UPDATE_USER_SUCCESS,
            payload: {
                updatedUser: updatedUser.data
            }
        });
    } catch (err) {
        console.error(err.message);
    }
}

function* watchUpdateUser() {
    yield takeEvery(UserActions.UPDATE_USER, updateUser);
}

function* deleteUser(action) {
    try {
        const deletedUser = yield call(
            axios.delete,
            `${api.url}/users/${action.payload.deletedUser.id}`
        );
        
        yield put({
            type: UserActions.DELETE_USER_SUCCESS,
            payload: {
                deletedUser: deletedUser.data
            }
        });
    } catch (err) {
        console.error(err.message);
    }
}

function* watchDeleteUser() {
    yield takeEvery(UserActions.DELETE_USER, deleteUser);
}

export default function* userSagas() {
    yield all([
        watchGetUser(),
        watchAddUser(),
        watchUpdateUser(),
        watchDeleteUser()
    ]);
}