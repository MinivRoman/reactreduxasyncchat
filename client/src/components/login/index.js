import React from 'react';
import './login.css';

class Login extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            login: '',
            password: ''
        }
    }

    onChangeLogin = (login) => this.setState({ login });
    onChangePassword = (password) => this.setState({ password });

    onClickSubmit = (e) => {
        e.preventDefault();

        const { login, password } = this.state;
        const data = {
            login,
            password
        };

        fetch("/api/login", {
            method: "POST",
            headers: {
                "Accept": "application/json",
                "Content-type": "application/json"
            },
            body: JSON.stringify(data)
        })
            .then((res) => res.json())
            .then((data) => {
                const { login } = data.payload;
                sessionStorage.setItem("jwt", data.jwt);

                if (login === "admin") {
                    this.props.history.push("/userList");
                } else {
                    this.props.history.push("/chat");
                }
            }).catch((err) => {
                console.error(err);
            });
    }

    render() {
        return (
            <div className="login">
                <form className="login-form">
                    <input type="text" placeholder="login" onChange={(e) => this.onChangeLogin(e.target.value)} />
                    <input type="password" placeholder="password" onChange={(e) => this.onChangePassword(e.target.value)} />
                    <input type="submit" onClick={this.onClickSubmit} />
                </form>
            </div>
        );
    }
}

export default Login;