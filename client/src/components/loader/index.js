import React from 'react';
import Loader from 'react-loader-spinner';

export default class loader extends React.Component {
    render() {
        return <Loader type="Rings" color="aqua" height={80} width={80} />;
    }
}