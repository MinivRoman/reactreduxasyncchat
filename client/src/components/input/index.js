import React from 'react';

const Input = ({ value, type, label, onChange }) => {
    return (
        <div className="item">
            <label className="">{label}</label>
            <input
                className=""
                value={value}
                type={type}
                name={label}
                onChange={onChange}
            />
        </div>
    );
}

export default Input;