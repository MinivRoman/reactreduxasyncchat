const checkAccess = (history) => {
    const jwt = sessionStorage.getItem('jwt');

    fetch("/api/login", {
        method: "GET",
        headers: {
            "Accept": "application/json",
            "Content-type": "application/json",
            "Authorization": `Bearer ${jwt}`
        }
    })
        .then((res) => res.json())
        .then(data => {
            console.log(data);
            if (data.err) {
                console.error(data.err);
                history.push("/");
            }
        }).catch((err) => {
            console.error(err);
            history.push("/");
        });
}

export default checkAccess;