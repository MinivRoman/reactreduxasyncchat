import { all } from 'redux-saga/effects';
import userSagas from '../components/User/sagas';
import messageSagas from '../components/Chat/sagas';

export default function* rootSaga() {
    yield all([
        userSagas(),
        messageSagas()
    ]);
}